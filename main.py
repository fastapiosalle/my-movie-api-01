from fastapi import FastAPI, Body, Path, Query, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.error_handler import ErrorHandler
from utils.jwt_bearer import JWTBearer
from routers.movie import movie_router
from routers.user import user_router

app = FastAPI()
app.title = "Champurrado 🥵"
app.version = "Trabajando en la sex update"
app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

@app.get("/", tags=['Sex UPDATE'])
def message():
    return HTMLResponse('<h1 style="color:red"> Hello Wolrd! </h1>')

compus = [
    
    {
        "id":0,
        "marca":"Hp",
        "color":"Plateada",
        "modelo":"Gp-200",
        "ram":"300 GB",
        "storage":"100 TB"

    },
    {
        "id":1,
        "marca":"Hp",
        "color":"Negra",
        "modelo":"Gp-600",
        "ram":"3 GB",
        "storage":"250 GB"

    },
    {
        "id":2,
        "marca":"Apple",
        "color":"Blanca",
        "modelo":"Macintosh 500",
        "ram":"16 GB",
        "storage":"500 GB"

    },
    {
        "id":3,
        "marca":"Apple",
        "color":"Negra",
        "modelo":"Macintosh 600",
        "ram":"32 Gb",
        "storage":"1 TB"

    },
    {
        "id":4,
        "marca":"MSI",
        "color":"Gris",
        "modelo":"KATANA KAI MINUS 0 DELUXE",
        "ram":"320 GB",
        "storage":"200 TB"

    }
]

@app.get('/Compus', tags=['Compus'])
def GetById(id: int = Query(min_length=0, max_length=200)):
    for item in compus:
        if item["id"] == id:
            return item
    return []

@app.get('/Compus/marca', tags=['Compus'])
def GetByMarca(Marca: str):
    lista = []
    for item in compus:
        if item['marca'] == Marca:
            lista.append(item)
    return lista

@app.get('/Compus/all', tags=['Compus'])
def get_compus():
    return compus

@app.post('/Compus/create', tags=['Compus'])
def create_compu(id:int = Body(), marca:str = Body(), modelo:str = Body(), color:str = Body(), ram:str = Body(), storage:str = Body()):
    modelo: str = Body()
    marca: str = Body()
    color: str = Body()
    ram: str = Body()
    storage: str = Body()

    compus.append({
        "id":id,
        "marca":marca,
        "color":color,
        "modelo": modelo,
        "ram":ram,
        "storage":storage
    })
    return compus

@app.put('/Compus/update', tags=['Compus'])
def update_compu(id:int = Body(), marca: str = Body(), color: str = Body(), modelo = Body(), ram = Body(), storage = Body()):
    for item in compus:
        if item["id"] == id:
            item["marca"] = marca
            item["color"] = color
            item["modelo"] = modelo
            item["ram"] = ram
            item["storage"] = storage
    return compus
            
@app.delete('/Compus/delete', tags=['Compus'])
def delete_compu(id: int):
    for item in compus:
        if item['id'] == id:
            compus.remove(item)
    return compus        
