
from models.movie import Movie as MovieModel

class MovieService():
    def __init__(self, db) -> None:
        self.db = db

    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return result

    def get_id(self, id: int):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result
    
    def get_movies_by_category(self, category):
        result = self.db
        return result

    def delete_movie(self, id):
        result = self.db.query(MovieModel).filter(MovieModel.id == id)
        return result