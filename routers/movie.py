from fastapi import Path, Query, HTTPException, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from utils.jwt_bearer import JWTBearer
from fastapi import APIRouter
from services.movie import MovieService

movie_router = APIRouter()

#region //*Clase 
class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(default="Diles que yo me se tus poses favoritas, que nananan y eso te excita, para hacerte veniiiiiiiiiiir", min_length=5, max_length=15)
    overview: str = Field(default="Descr")
    year: int = Field(le=2024)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=2)

    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "title": "Mi pelicula",
                "overview":"Descripción de pelicula",
                "year": 2024,
                "rating": 6.6,
                "category": "Accion"
            }
        }
#endregion

@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200)
def get_movies():
    db = Session()
    movies = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(movies))

@movie_router.get('/movies/{id}', tags=['movies'], response_model= list[Movie], dependencies=[Depends(JWTBearer())])
def get_movie(id: int = Path(ge=1, le=2000)):
    db = Session()
    movie = db.query(MovieModel).filter(MovieModel.id == id).first()
    if movie:
        return JSONResponse(status_code=200, content=jsonable_encoder(movie))
    else:
        raise HTTPException(status_code=404, detail="Película no encontrada")

@movie_router.post('/movies', tags=['movies'], dependencies=[Depends(JWTBearer())])
def create_movie(movie: Movie):
    db = Session()
    new_movie = MovieModel(**movie.model_dump())
    db.add(new_movie)
    db.commit()
    return JSONResponse(content={"message":"Se ha registrado la pelicula"}) 

@movie_router.put('/movies/{id}', tags=['movies'], dependencies=[Depends(JWTBearer())])
def update_movies(id:int, movie:Movie) -> dict:
    db = Session()
    db_movie = db.query(MovieModel).filter(MovieModel.id == id).first()
    if db_movie:
        db_movie.title = movie.title
        db_movie.overview = movie.overview
        db_movie.category = movie.category
        db_movie.year = movie.year
        db_movie.rating = db_movie.rating
        db.commit()
        db.refresh(db_movie)
        return {"message": "Se ha modificado la película", "data": jsonable_encoder(db_movie)}
    else:
        raise HTTPException(status_code=404, detail="Película no encontrada")

            
@movie_router.delete('/movies/{id}', tags=['movies'], dependencies=[Depends(JWTBearer())])
def delete_movie(id: int):
    db = Session()
    db_movie = db.query(MovieModel).filter(MovieModel.id == id).first()
    if db_movie:
        db.delete(db_movie)
        db.commit()
        return {"message": "Se ha eliminado la película"}
    else:
        raise HTTPException(status_code=404, detail="Película no encontrada")
